-- Create the dynfield for invoices to store if the invoice was transferred to HMD
-- Copyright 2016, cognovis GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

alter table im_invoices add exported_to_hmd_p char(1) default('f') constraint im_invoices_exported_hmd_p check (exported_to_hmd_p in ('t','f'));

select im_dynfield_attribute__new (
		null,                   -- widget_id
		'im_dynfield_attribute', -- object_type
		now(),                  -- creation_date
		null,                   -- creation_user
		null,                   -- creation_ip
		null,                   -- context_id
		'im_invoice',           -- attribute_object_type
		'exported_to_hmd_p',       -- attribute name
		0,
		0,
		null,
  	    'boolean',
		'#intranet-hmd.ExportedP#',      -- pretty name
		'#intranet-hmd.ExportedP#',      -- pretty plural
		'checkbox',              -- Widget (dummy)
		'f',
		'f'
);

-- Assume all old ones where exported manually somehow
ALTER TABLE im_invoices DISABLE TRIGGER im_invoices_tsearch_tr;
update im_invoices set exported_to_hmd_p = 't';
ALTER TABLE im_invoices ENABLE TRIGGER im_invoices_tsearch_tr;

create table im_hmd_payments (
	erfnr			integer not null,
	betrag			numeric not null,
	gkonto			integer not null,
	datum			timestamptz,
	text			text
)