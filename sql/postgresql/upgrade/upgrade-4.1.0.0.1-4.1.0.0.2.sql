-- Create the dynfield for invoices to store if the invoice was transferred to HMD
-- Copyright 2016, cognovis GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
SELECT acs_log__debug('/packages/intranet-hmd/sql/postgresql/upgrade/upgrade-4.1.0.0.1-4.1.0.0.2.sql','');

create table im_hmd_payments (
	erfnr			integer not null,
	betrag			numeric not null,
	gkonto			integer not null,
	datum			timestamptz,
	text			text
)