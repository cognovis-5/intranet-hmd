# /package/intranet-hmd/download-invoices.tcl
#
# Copyright (C) 2016 cognovis GmbH

ad_page_contract {
    Download the HMD FBASC CSV File.

    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    @creation-date November 2016
} {
    {return_url ""}
}

set user_id [ad_maybe_redirect_for_registration]
set page_title "Download FBASC"

set current_date [ns_localsqltimestamp]


# ---------------------------------------------------------------
# Provider bills
# ---------------------------------------------------------------

set invoice_ids [db_list export_invoices "select c.cost_id from im_costs c,im_invoices i where cost_type_id in ([ns_dbquotelist [list [im_cost_type_bill] [im_cost_type_correction_bill]]]) and i.invoice_id = c.cost_id and i.exported_to_hmd_p = 'f'"]

set csv_contents [list]
set failed_invoice_ids [list]
foreach invoice_id $invoice_ids {

    # HMD expects carriage return and linefeed
    set content "[intranet_hmd::bill_csv -invoice_id $invoice_id]"
    if {$content eq ""} {
        lappend failed_invoice_ids $invoice_id
    } else {
        lappend csv_contents $content
        db_dml mark_exported "update im_invoices set exported_to_hmd_p = 't' where invoice_id = :invoice_id"
        db_dml exported_date "update im_invoices set exported_to_hmd_date = :current_date where invoice_id=:invoice_id"

    }
}

set csv_content [join $csv_contents "\r\n"]

set fbasc_csv [ns_mktemp]
set file [open $fbasc_csv w]
fconfigure $file -encoding "iso8859-1"
puts $file $csv_content
flush $file
close $file


# Return the file
set outputheaders [ns_conn outputheaders]
ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"Provider.fbasc.hia\""
ns_returnfile 200 text/text $fbasc_csv
acs_mail_lite::send -to_addr malte.sussdorff@cognovis.de -subject "Failed HMD Export" "Invoices $failed_invoice_ids could not be downloaded"

file delete -force $fbasc_csv

