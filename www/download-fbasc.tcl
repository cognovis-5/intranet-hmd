# /package/intranet-hmd/download-invoices.tcl
#
# Copyright (C) 2016 cognovis GmbH

ad_page_contract {
    Download the HMD FBASC CSV File.

    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    @creation-date November 2016
} {
    {return_url ""}
}

set user_id [ad_maybe_redirect_for_registration]
set page_title "Download FBASC"

set current_date [ns_localsqltimestamp]


set context_bar [im_context_bar [list "/intranet-hmd" "HMD"] "Download FBASC"]

set form_id "hmd-download-fbasc"

set interco_options [db_list_of_lists interco "select c.company_name, c.company_id from im_companies c where c.company_status_id in (select * from im_sub_categories(46)) and c.company_type_id = 11000000 order by lower(c.company_name)"]

#    {since_date:date(date) {label "Since Date"} {after_html {<input type="button" style="height:23px; width:23px; background: url('/resources/acs-templating/calendar.gif');" onclick ="return showCalendarWithDateWidget('start_date', 'y-m-d');" >}}}

ad_form -name $form_id -html { enctype multipart/form-data } -action /intranet-hmd/download-fbasc -cancel_url $return_url -form {
    {interco_company_id:text(select) 
        {label "Intercompany Company Id"} 
        {options $interco_options}
    }

} -on_submit {
    
    # ---------------------------------------------------------------
    # Get the list of invoices
    # ---------------------------------------------------------------
    set invoice_ids [db_list export_invoices "select c.cost_id from im_costs c,im_projects p,im_invoices i where cost_type_id in ([im_cost_type_invoice],[im_cost_type_correction_invoice]) and p.project_id = c.project_id and p.interco_company_id = :interco_company_id and i.invoice_id = c.cost_id and i.exported_to_hmd_p = 'f'"]
    
    set csv_contents [list]
    set failed_invoice_ids [list]
    foreach invoice_id $invoice_ids {
        
        # HMD expects carriage return and linefeed
        set content "[intranet_hmd::invoice_csv -invoice_id $invoice_id]"
        if {$content eq ""} {
            lappend failed_invoice_ids $invoice_id
        } else {
            lappend csv_contents $content
            db_dml mark_exported "update im_invoices set exported_to_hmd_p = 't' where invoice_id = :invoice_id"
            db_dml exported_date "update im_invoices set exported_to_hmd_date = :current_date where invoice_id=:invoice_id"

        }
    }
    set csv_content [join $csv_contents "\r\n"]

    set fbasc_csv [ns_mktemp]
    set file [open $fbasc_csv w]
    fconfigure $file -encoding "iso8859-1"
    puts $file $csv_content
    flush $file
    close $file


    # Return the file
    set interco_company [db_string company "select company_name from im_companies where company_id = :interco_company_id"]

    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"${interco_company}.fbasc.hia\""
    ns_returnfile 200 text/text $fbasc_csv
    acs_mail_lite::send -to_addr [ad_system_owner] -from_addr [ad_system_owner] -subject "Failed HMD Export" -body "Invoices $failed_invoice_ids could not be downloaded"
} -after_submit {
    file delete -force $fbasc_csv
}
