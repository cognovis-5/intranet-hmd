# /package/intranet-hmd/upload-revenue_aktuell.tcl
#
# Copyright (C) 2016 cognovis GmbH

ad_page_contract {
    Serve the user a form to upload an XLS from the Revenue HMD
    
    Converts it to a CSV and runs reality checks on it

    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    @author etm
    @creation-date November 2016
} {
    {return_url ""}
}

set user_id [ad_maybe_redirect_for_registration]
set page_title "Upload Revenue from HMD"

set context_bar [im_context_bar [list "/intranet-hmd" "HMD"] "HMD companies"]

set form_id "hmd-upload-revenue_aktuell2"

ad_form -name $form_id -html { enctype multipart/form-data } -action /intranet-hmd/upload-revenue_aktuell2 -cancel_url $return_url -form {
    {upload_file:file(file),optional
        {label "#acs-subsite.Filename#"}
        {help_text "Upload the XLS FILE HERE"}
    }
    {payment_type_id:text(im_category_tree) 
        {label "#intranet-payments.Payment_Type#"}
        {custom {category_type "Intranet Payment Type"}}
    }
} -on_submit {
	regsub {xls.*} $upload_file {xls} filename
	set tmp_filename [template::util::file::get_property tmp_filename $upload_file]
    
    set revenue_lines [intranet_oo::xls_to_csv -xls_filename $tmp_filename]

        # revenue_currency check
     set payment_type [im_category_from_id $payment_type_id]
    # if {[regexp {[*CHF]$} $payment_type]} { set revenue_currency "CHF" } else {set revenue_currency "EUR"}
    	
	set log_html ""
	set costtab ""
    set linecount 0
    set header_line_found_p 0
    foreach line $revenue_lines {
        if {$header_line_found_p eq 0} {
            # Search for the header line
            if {[lsearch $line "Datum"]<0} {
                continue
            } else {
                set date_idx [lsearch $line "Datum"]
                set company_id_idx [lsearch $line "Gegenkonto"]
                set erfnr_idx [lsearch $line "Erf.Nr."]
                set revenue_comment_idx [lsearch $line "Erfassungstext"]
                set soll_idx [lsearch $line "Soll"]
                set haben_idx [lsearch $line "Haben"]
                set beleg_idx [lsearch $line "Beleg Nr"]
                set cost_idx [lsearch $line "cost_id"]
                set revenue_currency_idx [lsearch $line "Waehrung"]
                set header_line_found_p 1
                continue
            }
        }
		set linecount [expr $linecount+1]
       
##COMPANY###  Check if we have a company_id in there
        set company_id [lindex $line $company_id_idx]
        set erfnr [lindex $line $erfnr_idx]
       # if {$company_id eq ""} {continue}
       # if {![string is digit $company_id]} {continue}
        
        
        # find the invoice_number
        set invoice_found_p 0
        set revenue_comment [lindex $line $revenue_comment_idx]
        set revenue_comment "$revenue_comment (ErfNr: $erfnr)"
        # remove the excel formating
        set revenue_amount [lindex $line $soll_idx]
        set rev_habensoll "E" 
        if {$revenue_amount eq ""} {
		set revenue_amount [lindex $line $haben_idx]
		set rev_habensoll "A" 
        }
        
        
        set costid_list [lindex $line $cost_idx]
	   set beleg [lindex $line $beleg_idx]
	   
	   set revenue_currency [lindex $line $revenue_currency_idx]
	   
	   #change , in amount for .
        regsub -all {,} $revenue_amount {} revenue_amount
        set revenue_amount [expr {double(round(100*$revenue_amount))/100}]
        
    	ns_log Debug "date_idx: $date_idx "
    
# Handle different date formats....
	if {[regexp {[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]} $date_idx]} {
		set received_date $date_idx
		} else { 

			regsub -all {\.} [lindex $line $date_idx] {/} revenue_date
			
			set received_date_stubs [split $revenue_date "/"]
			set received_date "[lindex $received_date_stubs 2]-[lindex $received_date_stubs 1]-[lindex $received_date_stubs 0]"
		}
    
    ns_log Debug "received_date: $received_date "
    
    
    
	   set nocompany 0
        
	### find company_id in database
        if {[db_0or1row company_id "select im_name_from_id(company_id) as customer_name, company_type_id from im_companies where company_id = :company_id"] eq 0} {
         set nocompany 1 }
        if {$company_id eq ""} { set nocompany 1}
        if {![string is digit $company_id]} { set nocompany 1}
        
        
	   if { $nocompany eq 1}	{
        	 ### record unknown company   
        	if { "1590" == $company_id } {
			set hmdtext "$payment_type_id $revenue_comment"
			db_dml record_error "insert into im_hmd_payments (erfnr,betrag,gkonto,datum,text) values (:erfnr,:revenue_amount,:company_id,:received_date,:hmdtext)"
		   }
		   
		set logstatus "<font color=black>no company found</font>"
		set costtab "<a>no company found</a>"
		set paytab "<a>no company found</a>"
		#background grey
		append log_html "<tr bgcolor=#BEBEBE>
							<td>$erfnr</td>
							<td>$logstatus</td>
							<td>00</td>
							<td>$company_id</td>
							<td></td>
							<td>$company_id</td>
							<td><strong>$revenue_currency $revenue_amount</strong><br>$received_date</td>	
							<td>$rev_habensoll</td>
							<td><nobr>$costtab</nobr></td>
							<td>$paytab</td>
							<td>$revenue_comment</td>
					</tr>"
        
        #set invoice_found_p 1
        continue
        }
        
        set logstatus ""
	   set costtab ""
	   set paytab ""
        set company_url [export_vars -base "/intranet/companies/view" -url {company_id}]
	   
	   ### since in some cases provider_id differs from customer_id check for company_type first
	   set company_type_id [db_string company_type_id "select company_type_id from im_companies where company_id = :company_id" -default ""]
        set customerlist "11000010 11000011"
        if {$company_type_id in $customerlist} { 
		set compOrProv company_id
		set costcompOrProv customer_id		
	   } else {
		set compOrProv  provider_id 
		set costcompOrProv provider_id 
		}
        
        if {$compOrProv == "company_id" } {
 		set compINVBILL_url [export_vars -base "/intranet-invoices/list?form_mode=edit&form_id=invoice_filter&__confirmed_p=0&__refreshing_p=0&start_idx=0&order_by=Customer&how_many=1500&cost_type_id=3700&cost_status_id=&start_date=&end_date=&view_name=invoice_list" -url {company_id}]
		} else { 
			set compINVBILL_url [export_vars -base "/intranet-invoices/list?form_mode=edit&form_id=invoice_filter&__confirmed_p=0&__refreshing_p=0&start_idx=0&order_by=Customer&how_many=1500&cost_type_id=3704&cost_status_id=&start_date=&end_date=&view_name=invoice_list" -url {company_id}]
		}
	   
	   if {$compOrProv == "company_id" && $rev_habensoll == "A"} {set revenue_amount [expr $revenue_amount * -1 ]}
        if {$compOrProv == "provider_id" && $rev_habensoll == "E"} {set revenue_amount [expr $revenue_amount * -1 ]}
	   

	   #if {$cost_status_id eq [im_cost_status_replaced]} {continue}			
            set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id}]


            
  
		  set paidsource ""
		  set logstatus ""
		  set paytab ""
		  set costtab ""
		  set foundORguess ""
		  set billexists ""
		  set bill_name ""
	  
	  
### -1- ### cost_id found
	set invoicecount 0
	if {[db_0or1row costid "select cost_id, cost_id as invoice_id, cost_name, 
					cost_type_id, cost_status_id, im_name_from_id(cost_status_id) as cost_status,
					coalesce(amount,0) as amount, currency, currency as costcurrency,
					coalesce(vat_amount,0) as vat_amount,  
					coalesce(paid_amount,0) as paid_amount, paid_currency,
					to_char(effective_date, 'YYYY-MM-DD') as cost_date,
					coalesce(amount-coalesce(paid_amount,0)) as missing_amount 
				from im_costs 
				where cost_id = :costid_list
					 and cost_status_id <> 3813"] ne 0} {
					 
		 if {![im_cost_type_is_invoice_or_bill_p $cost_type_id]} {continue}
            
            set total_amount [expr $amount + $vat_amount]
            set total_amount [expr {double(round(100*$total_amount))/100}]
            set costcurrency $currency
		  set costamount $total_amount
		
		# check if this is one of the company's cost 
	     if {[db_0or1row  costcompany_p "select * from im_costs where cost_id = :costid_list and $costcompOrProv = :company_id"] eq 0} {continue}     
	     
	     # check if payment exists
	     #set paymcount 0
		set payment_exists_p [db_string payment "select count(cost_id) from im_payments where cost_id = :cost_id" -default 0]
		
		if {$payment_exists_p ne 0} {			
			set innerpaytab ""
			db_foreach payms {select to_char(p.received_date, 'YYYY-MM-DD') as p_paid_date,  coalesce(amount,0) as p_paid_amount, currency as p_paid_currency 
							FROM im_payments p WHERE p.cost_id = :cost_id} {
			append innerpaytab "<li>$p_paid_currency $p_paid_amount $p_paid_date</li>"
			}			
							
					
						
			#ns_log Debug "received_date: $received_date \n paid_date: $paid_date"
               
			set paidsource "p"
			set paid_amount [expr $paid_amount + 0]
			set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
			set addpayment_url [export_vars -base "/intranet-cust-fud/record-payment" -url {{cost_id $invoice_id} {note $revenue_comment} {received_date $received_date} {amount $revenue_amount} payment_type_id {payment_currency $revenue_currency}}]
			set notetotal "$revenue_comment ( Total paid: $revenue_amount )"
			set addpartpayment_url [export_vars -base "/intranet-cust-fud/record-payment" -url {{cost_id $invoice_id} {note $notetotal} {received_date $received_date} {amount $total_amount} payment_type_id {payment_currency $revenue_currency}}]
			set set2paid_url [export_vars -base "/intranet-cust-fud/cost_status" -url {{cost_status_id 3810} {invoice_id $invoice_id} return_url}]
			if {$revenue_amount ne $costamount } { set costamount "<strong><font color=red>$costamount</font></strong>" }
			set set2paid_url [export_vars -base "/intranet-cust-fud/cost_status" -url {{cost_status_id 3810} {invoice_id $invoice_id} return_url}]
			switch $cost_status_id {
				3813 { set cost_status "<strong><font color=red>$cost_status</font></strong>" }
				3810 { set cost_status "<strong>$cost_status</strong>" }
			}
			set costtab  "<a href='${invoice_url}' target=\"_blank\">$cost_name </a> $cost_id ($cost_status)<br>$costcurrency $costamount $cost_date"
			set costtab "$costtab  <br>
				<nobr><a href='${addpayment_url}'  target=\"_blank\">addRevAm</a> | <a href='${addpartpayment_url}'  target=\"_blank\">addCOST</a> | <a href='${set2paid_url}'  target=\"_blank\">set2paid</a></nobr>"
			set paytab "$innerpaytab"
			#set paytab "$paytab"
               if {$total_amount ne $paid_amount || $revenue_amount ne $costamount}	{ 
					set logstatus "<font color=red>CHECK-PaymExists</font>"
					set backcolor "#ffe6e6" 
			} else {  set logstatus "PaymExists" 
					set backcolor "white" 
			
			}

			
			#background light red or white
               append log_html "<tr bgcolor=$backcolor>
								<td>$erfnr</td>
								<td>$logstatus</td>
								<td>CP-1</td>
								<td><a href='${company_url}' target=\"_blank\">$customer_name</a></td>
								<td><a href='${compINVBILL_url}' target=\"_blank\">ListAll</a></td>
								<td>$company_id</td>
								<td><strong>$revenue_currency $revenue_amount</strong><br>$received_date</td>	
								<td>$rev_habensoll</td>
								<td><nobr>$costtab</nobr></td>
								<td>$paytab</td>
								<td>$revenue_comment</td>
						  </tr>"
			ns_log Debug "existing payment for $cost_name for $total_amount with $revenue_amount on $received_date" 
			####db_dml record_error "insert into im_hmd_payments (erfnr,betrag,gkonto,datum,text) values (:erfnr,:revenue_amount,:company_id,:received_date,:revenue_comment)"
			set invoice_found_p 1
		
	  
		} else  {
				# Add payment if there is still some amount due to pay 
                    etm_payment_create_payment -cost_id $invoice_id -note $revenue_comment -received_date $received_date -actual_amount $revenue_amount -payment_type_id $payment_type_id
				set logstatus "<font color=black>Success_costid</font>"
				db_1row cost "select * from im_costs where cost_id = :cost_id"
				set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
				set set2paid_url [export_vars -base "/intranet-cust-fud/cost_status" -url {{cost_status_id 3810} {invoice_id $invoice_id} return_url}]
				set costtab  "<a href='${invoice_url}' target=\"_blank\">$cost_name </a> $cost_id ($cost_status)<br>$costcurrency $costamount $cost_date <br> <a href='${set2paid_url}'  target=\"_blank\">set2paid</a></nobr>"
				if {$revenue_amount ne $costamount } { set costamount "<strong><font color=red>$costamount</font></strong>" }
				set paidsource "c"
				set paytab "$paid_currency $paid_amount ($paidsource)"
				# background green
				append log_html "<tr bgcolor=#a9dfbf>
								<td>$erfnr</td>
								<td>$logstatus</td>
								<td>CID-2</td>
								<td><a href='${company_url}' target=\"_blank\">$customer_name</a></td>
								<td><a href='${compINVBILL_url}' target=\"_blank\">ListAll</a></td>
								<td>$company_id</td>
								<td><strong>$revenue_currency $revenue_amount</strong><br>$received_date</td>
								<td>$rev_habensoll</td>
								<td><nobr>$costtab</nobr></td>
								<td>$paytab</td>
								<td>$revenue_comment</td>
							  </tr>"
                    ns_log Debug "HMD Revenue Upload found UNPAID invoice $cost_name for $revenue_amount"
                                    set invoice_found_p 1

		}
		 set invoicecount 1
		 continue
		 set invoice_found_p 1

	  }
	  
	  

############################ find all costs per company_id  #############################################
           db_foreach invoice {select cost_id, cost_id as invoice_id, cost_name, 
					cost_type_id, cost_status_id, im_name_from_id(cost_status_id) as cost_status,
					coalesce(amount,0) as amount, currency,currency as costcurrency,
					coalesce(vat_amount,0) as vat_amount,  
					coalesce(paid_amount,0) as paid_amount, paid_currency,
					to_char(effective_date, 'YYYY-MM-DD') as cost_date,
					coalesce(amount-coalesce(paid_amount,0)) as missing_amount 
				from im_costs 
				where customer_id = :company_id or provider_id = :company_id
					 and cost_status_id <> 3813
				} {
				
				
				
		  #if {$cost_status_id eq [im_cost_status_replaced]} {continue}			
            set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id}]

            if {![im_cost_type_is_invoice_or_bill_p $cost_type_id]} {continue}
            
            set total_amount [expr $amount + $vat_amount]
            set total_amount [expr {double(round(100*$total_amount))/100}]
            
            set costcurrency $currency
		  set costamount $total_amount
		  set paidsource ""
		  set logstatus ""
		  set paytab ""
		  set costtab ""
		  set foundORguess ""
		  
		  set billexists ""
		  set bill_name ""

		  
		  set invoice_found_p 0
		

		  

### -2- ### check cost_name 
	 
	if {$invoice_found_p eq 0 } {
     #set costmatch [string match "*${cost_name}*" $revenue_comment]

     if {[string match "*${cost_name}*" $beleg]} {  
	     # check if payment exists
	     #set paymcount 0
		
		set payment_exists_p [db_string payment "select count(cost_id) from im_payments where (company_id = :company_id or provider_id = :company_id ) and cost_id = :cost_id" -default 0]
		
		if {$payment_exists_p} {			
		set innerpaytab ""
			db_foreach payms {select to_char(p.received_date, 'YYYY-MM-DD') as p_paid_date,  coalesce(amount,0) as p_paid_amount, currency as p_paid_currency 
							FROM im_payments p WHERE p.cost_id = :cost_id} {
			append innerpaytab "<li>$p_paid_currency $p_paid_amount $p_paid_date</li>"
			}			
							
							
						
			#ns_log Debug "received_date: $received_date \n paid_date: $paid_date"
               
			set paidsource "p"
			set paid_amount [expr $paid_amount + 0]
			set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
			set addpayment_url [export_vars -base "/intranet-cust-fud/record-payment" -url {{cost_id $invoice_id} {note $revenue_comment} {received_date $received_date} {amount $revenue_amount} payment_type_id {payment_currency $revenue_currency}}]
			set notetotal "$revenue_comment ( Total paid: $revenue_amount )"
			set addpartpayment_url [export_vars -base "/intranet-cust-fud/record-payment" -url {{cost_id $invoice_id} {note $notetotal} {received_date $received_date} {amount $total_amount} payment_type_id {payment_currency $revenue_currency}}]
			set set2paid_url [export_vars -base "/intranet-cust-fud/cost_status" -url {{cost_status_id 3810} {invoice_id $invoice_id} return_url}]
			switch $cost_status_id {
				3813 { set cost_status "<strong><font color=red>$cost_status</font></strong>" }
				3810 { set cost_status "<strong>$cost_status</strong>" }
			}
			set costtab "<a href='${invoice_url}' target=\"_blank\">$cost_name </a> $cost_id ($cost_status)<br>$costcurrency $costamount $cost_date"
			set costtab "$costtab  <br> 
					<nobr><a href='${addpayment_url}'  target=\"_blank\">addRevAm</a> | <a href='${addpartpayment_url}'  target=\"_blank\">addCOST</a> | <a href='${set2paid_url}'  target=\"_blank\">set2paid</a></nobr>"
			if {$revenue_amount ne $costamount } { set costamount "<strong><font color=red>$costamount</font></strong>" }
			set paytab "$innerpaytab"
			set paytab "$paytab"
               if {$total_amount ne $paid_amount}	{ 
					set logstatus "<font color=red>CHECK-PaymExists</font>"
					set backcolor "#ffe6e6" 
			} else {  set logstatus "PaymExists" 
					set backcolor "white" 
			
			}

			
			#background light red
               append log_html "<tr bgcolor=$backcolor>
								<td>$erfnr</td>
								<td>$logstatus</td>
								<td>CNP-3</td>
								<td><a href='${company_url}' target=\"_blank\">$customer_name</a></td>
								<td><a href='${compINVBILL_url}' target=\"_blank\">ListAll</a></td>
								<td>$company_id</td>
								<td><strong>$revenue_currency $revenue_amount</strong><br>$received_date</td>	
								<td>$rev_habensoll</td>
								<td><nobr>$costtab</nobr></td>
								<td>$paytab</td>
								<td>$revenue_comment</td>
						  </tr>"
			ns_log Debug "existing payment for $cost_name for $total_amount with $revenue_amount on $received_date" 
			####db_dml record_error "insert into im_hmd_payments (erfnr,betrag,gkonto,datum,text) values (:erfnr,:revenue_amount,:company_id,:received_date,:revenue_comment)"
			set invoice_found_p 1
		
	  
		} elseif {$revenue_amount eq $costamount } {
				set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
				# Add payment if there is still some amount due to pay 
                    etm_payment_create_payment -cost_id $invoice_id -note $revenue_comment -received_date $received_date -actual_amount $revenue_amount -payment_type_id $payment_type_id
				set logstatus "<font color=black>Success_costid</font>"
				db_1row cost "select * from im_costs where cost_id = :cost_id"
				set costtab "<a href='${invoice_url}' target=\"_blank\">$cost_name ( $cost_id )<br>$costcurrency $costamount $cost_date</a>"
				if {$revenue_amount ne $costamount } { set costamount "<strong><font color=red>$costamount</font></strong>" }
				set paidsource "c"
				set paytab "$paid_currency $paid_amount ($paidsource)"
				# background green
				append log_html "<tr bgcolor=#a9dfbf>
								<td>$erfnr</td>
								<td>$logstatus</td>
								<td>CN-41</td>
								<td><a href='${company_url}' target=\"_blank\">$customer_name</a></td>
								<td><a href='${compINVBILL_url}' target=\"_blank\">ListAll</a></td>
								<td>$company_id</td>
								<td><strong>$revenue_currency $revenue_amount</strong><br>$received_date</td>
								<td>$rev_habensoll</td>
								<td><nobr>$costtab</nobr></td>
								<td>$paytab</td>
								<td>$revenue_comment</td>
							  </tr>"
                    ns_log Debug "HMD Revenue Upload found UNPAID invoice $cost_name for $revenue_amount"
                                    set invoice_found_p 1
			} 
		
		
		 set invoicecount 1
		continue
	   }		  
       
	 }
	}
 
## - 3 - ### list all unpaid invoices and leave it to the user if the payment is created or print out "nothing found"
 	if {$invoice_found_p eq 0 } {
		
		set invoicecount 0
		set costwhere ""
		set sameamount_exists_p [db_string sameamountcost "select count(cost_id) from im_costs 
						   where cost_type_id in (3700,3704,3725) and (customer_id = :company_id or provider_id = :company_id) 
							and (amount+vat_amount) = :revenue_amount " -default 0]
							
							
		set opencost_exists_p [db_string opencost "select count(cost_id) from im_costs 
						   where cost_type_id in (3700,3704,3725) and (customer_id = :company_id or provider_id = :company_id) 
							and cost_status_id != [im_cost_status_paid]" -default 0]
		
		
		
		if {$sameamount_exists_p} {
	#### all costs with same amount as revenue amount		
			set costwhere "and (amount+vat_amount) = :revenue_amount" 
		
		 
		
			set innercosttab ""
			set innerpaytab ""
			set helpcosttab ""
			set helppaytab ""
			set notetotal ""
			
			db_foreach invoice "select cost_id, cost_id as invoice_id, cost_name, coalesce(amount,0) as amount, coalesce(vat_amount,0) as vat_amount, currency,
							coalesce(paid_amount,0) as paid_amount , paid_currency,to_char(effective_date, 'YYYY-MM-DD') as cost_date,
							im_category_from_id(cost_status_id) as cost_status,cost_status_id
							from im_costs 
							where cost_type_id in (3700,3704,3725) and (customer_id = :company_id or provider_id = :company_id) $costwhere order by effective_date" {
				#if {![im_cost_type_is_invoice_or_bill_p $cost_type_id]} {continue}
				set total_amount [expr $amount + $vat_amount]
				set total_amount [expr {double(round(100*$total_amount))/100}]
				set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
				if {$revenue_amount ne $costamount } { set costamount "<strong><font color=red>$costamount</font></strong>" }
				switch $cost_status_id {
					3813 { set cost_status "<strong><font color=red>$cost_status</font></strong>" }
					3810 { set cost_status "<strong>$cost_status</strong>" }
				}
				set helpcosttab "<li><a href='${invoice_url}' target=\"_blank\">$cost_name</a> $cost_id ($cost_status)<br>$currency $total_amount ($cost_date)"
				set helppaytab "$paid_currency $paid_amount"
				set addpayment_url [export_vars -base "/intranet-cust-fud/record-payment" -url {{cost_id $invoice_id} {note $revenue_comment} {received_date $received_date} {amount $revenue_amount} payment_type_id {payment_currency $revenue_currency}}]
				set set2paid_url [export_vars -base "/intranet-cust-fud/cost_status" -url {{cost_status_id 3810} {invoice_id $invoice_id} return_url}]
				set notetotal "$revenue_comment ( Total paid: $revenue_amount )"
				set addpartpayment_url [export_vars -base "/intranet-cust-fud/record-payment" -url {{cost_id $invoice_id} {note $notetotal} {received_date $received_date} {amount $total_amount} payment_type_id {payment_currency $revenue_currency}}]
				append innercosttab "$helpcosttab <br> 
								<nobr><a href='${addpayment_url}'  target=\"_blank\">addRevAm</a> | 
								<a href='${addpartpayment_url}'  target=\"_blank\">addCOST</a> | 
								<a href='${set2paid_url}'  target=\"_blank\">set2paid</a></nobr><br><br>" 
				append innerpaytab "<li>$helppaytab (${cost_name}) <br><br><br>" 
			}
			if {$innercosttab ne "" } {set costtab $innercosttab }
			if {$innerpaytab ne "" } {set paytab $innerpaytab } 
			

               set logstatus "<font color=black>CHECK-AllCost</font>"
               #background dark yellow
               append log_html "<tr bgcolor=#decc04>
								<td>$erfnr</td>
								<td>$logstatus</td>
								<td>SA</td>
								<td><a href='${company_url}' target=\"_blank\">$customer_name</a></td>
								<td><a href='${compINVBILL_url}' target=\"_blank\">ListAll</a></td>
								<td>$company_id</td>
								<td>$revenue_currency $revenue_amount<br>$received_date</td>	
								<td>$rev_habensoll</td>
								<td><nobr>$costtab</nobr></td>
								<td>$paytab</td>
								<td>$revenue_comment</td>
						 </tr>"
               set invoice_found_p 1
               
		} elseif {$opencost_exists_p} {
		### all open costs
		
		set costwhere "and cost_status_id != [im_cost_status_paid]"

			set innercosttab ""
			set innerpaytab ""
			set helpcosttab ""
			set helppaytab ""
			set notetotal ""
			
			db_foreach invoice "select cost_id, cost_id as invoice_id, cost_name, coalesce(amount,0) as amount, coalesce(vat_amount,0) as vat_amount, currency,
							coalesce(paid_amount,0) as paid_amount , paid_currency,to_char(effective_date, 'YYYY-MM-DD') as cost_date,
							im_category_from_id(cost_status_id) as cost_status,cost_status_id
							from im_costs 
							where cost_type_id in (3700,3704,3725) and (customer_id = :company_id or provider_id = :company_id) $costwhere order by effective_date" {
				#if {![im_cost_type_is_invoice_or_bill_p $cost_type_id]} {continue}
				set total_amount [expr $amount + $vat_amount]
				set total_amount [expr {double(round(100*$total_amount))/100}]
				set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id}]
				if {$revenue_amount ne $costamount } { set costamount "<strong><font color=red>$costamount</font></strong>" }
				switch $cost_status_id {
					3813 { set cost_status "<strong><font color=red>$cost_status</font></strong>" }
					3810 { set cost_status "<strong>$cost_status</strong>" }
				}				
				set helpcosttab "<li><a href='${invoice_url}' target=\"_blank\">$cost_name</a> $cost_id ($cost_status)<br>$currency $total_amount ($cost_date)"
				set helppaytab "$paid_currency $paid_amount"
				set addpayment_url [export_vars -base "/intranet-cust-fud/record-payment" -url {{cost_id $invoice_id} {note $revenue_comment} {received_date $received_date} {amount $revenue_amount} payment_type_id {payment_currency $revenue_currency}}]
				set set2paid_url [export_vars -base "/intranet-cust-fud/cost_status" -url {{cost_status_id 3810} {invoice_id $invoice_id} return_url}]
				set notetotal "$revenue_comment ( Total paid: $revenue_amount )"
				set addpartpayment_url [export_vars -base "/intranet-cust-fud/record-payment" -url {{cost_id $invoice_id} {note $notetotal} {received_date $received_date} {amount $total_amount} payment_type_id {payment_currency $revenue_currency}}]
				append innercosttab "$helpcosttab <br> 
								<nobr><a href='${addpayment_url}'  target=\"_blank\">addRevAm</a> | 
								<a href='${addpartpayment_url}'  target=\"_blank\">addCOST</a> | 
								<a href='${set2paid_url}'  target=\"_blank\">set2paid</a></nobr><br><br>" 
				append innerpaytab "<li>$helppaytab (${cost_name}) <br><br><br>" 
			}
			if {$innercosttab ne "" } {set costtab $innercosttab }
			if {$innerpaytab ne "" } {set paytab $innerpaytab } 
			

               set logstatus "<font color=black>CHECK-AllCost</font>"
               #background dark yellow
               append log_html "<tr bgcolor=#decc04>
								<td>$erfnr</td>
								<td>$logstatus</td>
								<td>AC</td>
								<td><a href='${company_url}' target=\"_blank\">$customer_name</a></td>
								<td><a href='${compINVBILL_url}' target=\"_blank\">ListAll</a></td>
								<td>$company_id</td>
								<td>$revenue_currency $revenue_amount<br>$received_date</td>	
								<td>$rev_habensoll</td>
								<td><nobr>$costtab</nobr></td>
								<td>$paytab</td>
								<td>$revenue_comment</td>
						 </tr>"
               set invoice_found_p 1
               
		}	else {
##### 4 ##########nothing found
			if {$invoice_found_p eq 0} {
				ns_log Debug "No unpaid invoice found for $customer_name :: $revenue_amount -- $revenue_comment"
				set logstatus "<font color=red>no unpaid invoice found</font>"
				set costtab "<a>no unpaid invoice found</a>"
				set paytab "<a>no unpaid invoice found</a>"
				#background light red
				append log_html "<tr bgcolor=#ffe6e6>
									<td>$erfnr</td>
									<td>$logstatus</td>
									<td>NF</td>
									<td><a href='${company_url}' target=\"_blank\">$customer_name</a></td>
									<td><a href='${compINVBILL_url}' target=\"_blank\">ListAll</a></td>
									<td>$company_id</td>
									<td><strong>$revenue_currency $revenue_amount</strong><br>$received_date</td>	
									<td>$rev_habensoll</td>
									<td><nobr>$costtab</nobr><br><a href='${compINVBILL_url}' target=\"_blank\">ListAll</a></td>
									<td>$paytab</td>
									<td>$revenue_comment</td>
							</tr>"


			# db_dml record_error "insert into im_hmd_payments (erfnr,betrag,gkonto,datum,text) values (:erfnr,:revenue_amount,:company_id,:received_date,:revenue_comment)"
			}            
		}
    }
  } 
} -after_submit {
    
    if {$log_html ne ""} {
		set log_html "
		<table>
		<tr>
		<th>ErfNr</th>
		<th>Status</th>
		<th>L</th>
		<th>Customer</th>
		<th>List</th>		
		<th>CustomerID</th>
		<th><nobr>Received     | </nobr></th>
		<th>HS___|</th>
		<th>CostName (G/F)</th>
		<th>existPaym</th>
		<th>Comment</th>
		</tr>
		$log_html
		</table>"

acs_mail_lite::send -to_addr fudadmin@gmail.com -cc_addr "webmin@fachuebersetzungsdienst.com" -from_addr [ad_system_owner] -subject "LOG Revenue Upload (Anzahl: $linecount ) $filename " -body "<html><body>$log_html</body></html>" -mime_type "text/html"
    }
    ad_return_error "Log" "$log_html <p /> "
}
