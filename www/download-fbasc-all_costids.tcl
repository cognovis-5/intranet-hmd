# /package/intranet-hmd/download-fbasc-all_variable.tcl
#
# Copyright (C) 2016 cognovis GmbH

ad_page_contract {
    Download the HMD FBASC CSV File for all companies

    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    @author etm
    @creation-date November 2016
} {
    {return_url ""}
}

set user_id [ad_maybe_redirect_for_registration]


# ---------------------------------------------------------------
# Customer invoices
# ---------------------------------------------------------------

set interco_options [db_list_of_lists interco "	select c.company_name, c.company_id 
										from im_companies c 
										where c.company_status_id in (select * from im_sub_categories(46)) 
											and c.company_type_id = 11000000 
										order by lower(c.company_name)"]

set zip_dir [ns_mktemp]
file mkdir $zip_dir
set file_created_p 0
set failed_invoice_ids [list]

        
        db_1row todays_date "
		select
			to_char(sysdate::date, 'YYYY') as todays_year,
			to_char(sysdate::date, 'MM') as todays_month,
			to_char(sysdate::date, 'DD') as todays_day
		from dual
		"
	
        set firstofmonth "$todays_year-$todays_month-01"
	   set today [db_string get_today_date "select sysdate from dual"]
        
# ---------------------------------------------------------------
# Get the list of invoices
# 


#         set invoice_ids [db_list export_invoices "select c.cost_id 
# 										from im_costs c,
# 											im_projects p,
# 											im_invoices i 
# 										where cost_type_id = :cost_type_id 
# 											and p.project_id = c.project_id 
# 											and p.interco_company_id = :company_id 
# 											and i.invoice_id = c.cost_id 
# 											--and i.exported_to_hmd_p = 'f'
# 											and effective_date > '2018-08-01' 
# 											and effective_date < '2018-09-01'"]




   set invoice_ids [db_list export_invoices "select c.cost_id 
									from im_costs c 
									where cost_id in ('3958246','3960997','3954754','3960830','3985763','3964986','3966027','3966839','3968517','3969000','3964226','3969036','3971981','3971889','3973862','3967994','3977718','3980441','3982016','3985217','3984001','3967204')"]

        set csv_contents [list]
        foreach invoice_id $invoice_ids {
            
            # HMD expects carriage return and linefeed
            set content "[intranet_hmd::invoice_check_csv -invoice_id $invoice_id]"
            if {$content eq ""} {
                lappend failed_invoice_ids $invoice_id
            } else {
                lappend csv_contents $content
                #db_dml mark_exported "update im_invoices set exported_to_hmd_p = 't', exported_to_hmd_date = :today where invoice_id = :invoice_id"
            }
        }
        
        if {$csv_contents ne ""} {
            set csv_content [join $csv_contents "\r\n"]
            
            set fbasc_csv [ns_mktemp]
            set file [open $fbasc_csv w]
            fconfigure $file -encoding "iso8859-1"
            puts $file $csv_content
            flush $file
            close $file
            file rename $fbasc_csv ${zip_dir}/1590.fbasc.csv
            set file_created_p 1
        }


# ---------------------------------------------------------------
# Provider bills
# ---------------------------------------------------------------

set invoice_ids [db_list export_invoices "select c.cost_id 
									from im_costs c 
									where cost_id in ('2369914','2393833','2393883','2460061','2466107','2470977','2631837','2745887','2733777','2737183','2753671','2749851')"]    
    
set csv_contents [list]
foreach invoice_id $invoice_ids {

	set today [db_string get_today_date "select sysdate from dual"]
    # HMD expects carriage return and linefeed
    set content "[intranet_hmd::bill_check_csv -invoice_id $invoice_id]"
    if {$content eq ""} {
        lappend failed_invoice_ids $invoice_id
    } else {
        lappend csv_contents $content
        #db_dml mark_exported "update im_invoices set exported_to_hmd_p = 't', exported_to_hmd_date = :today where invoice_id = :invoice_id"
    }
}
    
if {$csv_contents ne ""} {
    set csv_content [join $csv_contents "\r\n"]
    
    set fbasc_csv [ns_mktemp]
    set file [open $fbasc_csv w]
    fconfigure $file -encoding "iso8859-1"
    puts $file $csv_content
    flush $file
    close $file
    file rename $fbasc_csv ${zip_dir}/FUD.bills.fbasc.csv
    set file_created_p 1
}


if {$file_created_p} {
    # Generate the zipfile
    set zip_file [ns_mktemp]
    exec zip -r $zip_file ${zip_dir}
    
    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"FBASC.csv.zip\""
    ns_returnfile 200 application/zip ${zip_file}.zip
    
    acs_mail_lite::send -to_addr [ad_system_owner] -from_addr [ad_system_owner] -subject "Failed HMD Export" -body "Invoices $failed_invoice_ids could not be downloaded"
    
    file delete -force ${zip_file}.zip
    file delete -force $zip_dir
} else {
    ad_return_error "No new bookings" "No file generated, no new invoices found"
}