# /package/intranet-hmd/download-fbasc-all_variable.tcl
#
# Copyright (C) 2016 cognovis GmbH

ad_page_contract {
    Download the HMD FBASC CSV File for all companies

    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    @author etm
    @creation-date November 2016
} {
    {return_url ""}
}

set user_id [ad_maybe_redirect_for_registration]


# ---------------------------------------------------------------
# Customer invoices
# ---------------------------------------------------------------

set interco_options [db_list_of_lists interco "	select c.company_name, c.company_id 
										from im_companies c 
										where c.company_status_id in (select * from im_sub_categories(46)) 
											and c.company_type_id = 11000000 
										order by lower(c.company_name)"]

set zip_dir [ns_mktemp]
file mkdir $zip_dir
set file_created_p 0
set failed_invoice_ids [list]

foreach interco_option $interco_options {
    set company_id [lindex $interco_option 1]
    set company_name [lindex $interco_option 0]
    

    foreach cost_type_id [list [im_cost_type_invoice] [im_cost_type_correction_invoice]] {
        # ---------------------------------------------------------------
        # Get the list of invoices
        # ---------------------------------------------------------------
        
        db_1row todays_date "
		select
			to_char(sysdate::date, 'YYYY') as todays_year,
			to_char(sysdate::date, 'MM') as todays_month,
			to_char(sysdate::date, 'DD') as todays_day
		from dual
		"
	
        set firstofmonth "$todays_year-$todays_month-01"
	   set today [db_string get_today_date "select sysdate from dual"]
        
        set invoice_ids [db_list export_invoices "select c.cost_id 
										from im_costs c,
											im_projects p,
											im_invoices i 
										where cost_type_id = :cost_type_id 
											and p.project_id = c.project_id 
											and p.interco_company_id = :company_id 
											and i.invoice_id = c.cost_id 
											--and i.exported_to_hmd_p = 'f'
											and effective_date >= date_trunc('month', current_date - interval '2' month)
											and effective_date < date_trunc('month', current_date)"]

        set csv_contents [list]
        
         set csv_values [list "Kontonummer"]; # 0 Kontonummer
		lappend csv_values " Gegenkonto"; # 1 Gegenkonto
	     lappend csv_values "Bruttobetrag" ; # 2 Bruttobetrag
		lappend csv_values "Soll / Haben" ; # 3 Soll / Haben
		lappend csv_values "Belegnummer" ; # 4 Belegnummer
		lappend csv_values " Belegnummer 2" ; # 5 Belegnummer 2
		lappend csv_values "Steuerschlüssel" ; # 6 Steuerschlüssel
		lappend csv_values "Kostenstelle" ; # 7 Kostenstelle
		lappend csv_values "Kostenträger" ; # 8 Kostenträger
		lappend csv_values "Menge" ; # 9 Menge
		lappend csv_values "Belegdatum" ; # 10 Belegdatum
		lappend csv_values "Fähligkeitsdatum" ; # 11 Fähligkeitsdatum
		lappend csv_values "Buchungstext" ; # 12 Buchungstext
		lappend csv_values "Skontobetrag" ; # 13 Skontobetrag
		lappend csv_values "Steuerschlüssel Skonto" ; # 14 Steuerschlüssel Skonto
		lappend csv_values "Skontiziel" ; # 15 Skontiziel
		lappend csv_values "Skontierfähiger Betrag" ; # 16	 Skontierfähiger Betrag	
		lappend csv_values "Währung" ; # 17 Währung
		lappend csv_values "ID Nummer ZM Meldung" ; # 18 ID Nummer ZM Meldung
		lappend csv_values "Kontenart ((D)ebitor / (K)reditor / (S)achkonto)" ; # 19 Kontenart ((D)ebitor / (K)reditor / (S)achkonto)
		lappend csv_values "Suchnamen" ; # 20 Suchnamen
		lappend csv_values "Anrede" ; # 21 Anrede
		lappend csv_values "Vorname / Nachname" ; # 22 Vorname / Nachname
		lappend csv_values "Branche" ; # 23 Branche
		lappend csv_values "Strasse" ; # 24 Strasse
		lappend csv_values "Postleitzahl" ; # 25 Postleitzahl
		lappend csv_values "Ort" ; # 26 Ort
		lappend csv_values "Land" ; # 27 Land
		lappend csv_values "Kontonummer" ; # 28 Kontonummer
		lappend csv_values "Bankleitzahl" ; # 29 Bankleitzahl														
		lappend csv_values "Nettotage" ; # 30 Nettotage
		lappend csv_values "Skontotage" ; # 31 Skontotage
		lappend csv_values "Skontoprozent" ; # 32 Skontoprozent
		lappend csv_values "Telefonnummer" ; # 33 Telefonnummer
		lappend csv_values "Telefaxnummber" ; # 34 Telefaxnummber
		lappend csv_values "Sammelkonto" ; # 35 Sammelkonto
		lappend csv_values "Lastschrifteinzug" ; # 36 Lastschrifteinzug
		lappend csv_values "Selektion" ; # 37 Selektion
		lappend csv_values "Sachverhalt 13b" ; # 38 Sachverhalt 13b
		lappend csv_values "Dokument zur Erfassung" ; # 39 Dokument zur Erfassung
		lappend csv_values "IBAN" ; # 40 IBAN
		lappend csv_values "BIC" ; # 41 BIC
		lappend csv_values "Bankbezeichnung" ; # 42 Bankbezeichnung
		lappend csv_values "Vorname" ; # 43 Vorname
		lappend csv_values "Kundennummer" ; # 44 Kundennummer
		lappend csv_values "Freigabe Factoring" ; # 45 Freigabe Factoring
		lappend csv_values "Obligonummer" ; # 46	Obligonummer
		lappend csv_values "Mandatsreferenznummer" ; # 47 Mandatsreferenznummer
		lappend csv_values "Datum Mandatsreferenz" ; # 48 Datum Mandatsreferenz
		lappend csv_values "Limit" ; # 49 Limit
		lappend csv_values "Limit gültig ab" ; # 50 Limit gültig ab
		lappend csv_values "Konzern" ; # 51 Konzern



		set csv_line [::csv::join $csv_values ";"]
        lappend csv_contents $csv_line





        foreach invoice_id $invoice_ids {
            
            # HMD expects carriage return and linefeed
            set content "[intranet_hmd::invoice_check_csv -invoice_id $invoice_id]"
            if {$content eq ""} {
                lappend failed_invoice_ids $invoice_id
            } else {
                lappend csv_contents $content
                db_dml mark_exported "update im_invoices set exported_to_hmd_p = 't', exported_to_hmd_date = :today where invoice_id = :invoice_id"
            }
        }
        
        if {$csv_contents ne ""} {
            set csv_content [join $csv_contents "\r\n"]
            
            set fbasc_csv [ns_mktemp]
            set file [open $fbasc_csv w]
            fconfigure $file -encoding "iso8859-1"
            puts $file $csv_content
            flush $file
            close $file
            set lastmonth [clock format [clock scan "1 month ago" -base [clock seconds]] -format "%Y%m"]
 			set cost_type "AR"
            if { $cost_type_id eq [im_cost_type_correction_invoice] } { set cost_type "AR-Korr" }
            file rename $fbasc_csv ${zip_dir}/${company_name}.$cost_type.${lastmonth}_orig.csv
            set file_created_p 1
        }
    }
}

# ---------------------------------------------------------------
# Provider bills
# ---------------------------------------------------------------

set invoice_ids [db_list export_invoices  "
			select c.cost_id 
			from im_costs c,im_invoices i , acs_objects 
			where cost_type_id = [im_cost_type_bill] 
				 and i.invoice_id = c.cost_id 
				 and c.cost_id = object_id 
				 and creation_date  >= date_trunc('month', current_date - interval '2' month)
				 and creation_date < date_trunc('month', current_date)"]
    
set csv_contents [list]


set csv_values [list "Kontonummer"]; # 0 Kontonummer
lappend csv_values " Gegenkonto"; # 1 Gegenkonto
lappend csv_values "Bruttobetrag" ; # 2 Bruttobetrag
lappend csv_values "Soll / Haben" ; # 3 Soll / Haben
lappend csv_values "Belegnummer" ; # 4 Belegnummer
lappend csv_values " Belegnummer 2" ; # 5 Belegnummer 2
lappend csv_values "Steuerschlüssel" ; # 6 Steuerschlüssel
lappend csv_values "Kostenstelle" ; # 7 Kostenstelle
lappend csv_values "Kostenträger" ; # 8 Kostenträger
lappend csv_values "Menge" ; # 9 Menge
lappend csv_values "Belegdatum" ; # 10 Belegdatum
lappend csv_values "Fähligkeitsdatum" ; # 11 Fähligkeitsdatum
lappend csv_values "Buchungstext" ; # 12 Buchungstext
lappend csv_values "Skontobetrag" ; # 13 Skontobetrag
lappend csv_values "Steuerschlüssel Skonto" ; # 14 Steuerschlüssel Skonto
lappend csv_values "Skontiziel" ; # 15 Skontiziel
lappend csv_values "Skontierfähiger Betrag" ; # 16	 Skontierfähiger Betrag	
lappend csv_values "Währung" ; # 17 Währung
lappend csv_values "ID Nummer ZM Meldung" ; # 18 ID Nummer ZM Meldung
lappend csv_values "Kontenart ((D)ebitor / (K)reditor / (S)achkonto)" ; # 19 Kontenart ((D)ebitor / (K)reditor / (S)achkonto)
lappend csv_values "Suchnamen" ; # 20 Suchnamen
lappend csv_values "Anrede" ; # 21 Anrede
lappend csv_values "Vorname / Nachname" ; # 22 Vorname / Nachname
lappend csv_values "Branche" ; # 23 Branche
lappend csv_values "Strasse" ; # 24 Strasse
lappend csv_values "Postleitzahl" ; # 25 Postleitzahl
lappend csv_values "Ort" ; # 26 Ort
lappend csv_values "Land" ; # 27 Land
lappend csv_values "Kontonummer" ; # 28 Kontonummer
lappend csv_values "Bankleitzahl" ; # 29 Bankleitzahl														
lappend csv_values "Nettotage" ; # 30 Nettotage
lappend csv_values "Skontotage" ; # 31 Skontotage
lappend csv_values "Skontoprozent" ; # 32 Skontoprozent
lappend csv_values "Telefonnummer" ; # 33 Telefonnummer
lappend csv_values "Telefaxnummber" ; # 34 Telefaxnummber
lappend csv_values "Sammelkonto" ; # 35 Sammelkonto
lappend csv_values "Lastschrifteinzug" ; # 36 Lastschrifteinzug
lappend csv_values "Selektion" ; # 37 Selektion
lappend csv_values "Sachverhalt 13b" ; # 38 Sachverhalt 13b
lappend csv_values "Dokument zur Erfassung" ; # 39 Dokument zur Erfassung
lappend csv_values "IBAN" ; # 40 IBAN
lappend csv_values "BIC" ; # 41 BIC
lappend csv_values "Bankbezeichnung" ; # 42 Bankbezeichnung
lappend csv_values "Vorname" ; # 43 Vorname
lappend csv_values "Kundennummer" ; # 44 Kundennummer
lappend csv_values "Freigabe Factoring" ; # 45 Freigabe Factoring
lappend csv_values "Obligonummer" ; # 46	Obligonummer
lappend csv_values "Mandatsreferenznummer" ; # 47 Mandatsreferenznummer
lappend csv_values "Datum Mandatsreferenz" ; # 48 Datum Mandatsreferenz
lappend csv_values "Limit" ; # 49 Limit
lappend csv_values "Limit gültig ab" ; # 50 Limit gültig ab
lappend csv_values "Konzern" ; # 51 Konzern

set csv_line [::csv::join $csv_values ";"]
lappend csv_contents $csv_line


foreach invoice_id $invoice_ids {

	set today [db_string get_today_date "select sysdate from dual"]
    # HMD expects carriage return and linefeed
    set content "[intranet_hmd::bill_check_csv -invoice_id $invoice_id]"
    if {$content eq ""} {
        lappend failed_invoice_ids $invoice_id
    } else {
        lappend csv_contents $content
        db_dml mark_exported "update im_invoices set exported_to_hmd_p = 't', exported_to_hmd_date = :today where invoice_id = :invoice_id"
    }
}
    
if {$csv_contents ne ""} {
    set csv_content [join $csv_contents "\r\n"]
    
    set fbasc_csv [ns_mktemp]
    set file [open $fbasc_csv w]
    fconfigure $file -encoding "iso8859-1"
    puts $file $csv_content
    flush $file
    close $file
    set lastmonth [clock format [clock scan "1 month ago" -base [clock seconds]] -format "%Y%m"]
    file rename $fbasc_csv ${zip_dir}/FUD.ER.fbasc.${lastmonth}_orig.csv
    set file_created_p 1
}


if {$file_created_p} {
    # Generate the zipfile
    set zip_file [ns_mktemp]
    exec zip -r $zip_file ${zip_dir}
    
    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"FBASC_$lastmonth.csv.zip\""
    ns_returnfile 200 application/zip ${zip_file}.zip
    if {$failed_invoice_ids eq ""} {set failed_invoice_ids "none"}
    acs_mail_lite::send -to_addr [ad_system_owner] -from_addr [ad_system_owner] -subject "Report HMD Export" -body "Failed Invoices: $failed_invoice_ids "
    
    file delete -force ${zip_file}.zip
    file delete -force $zip_dir
} else {
    ad_return_error "No new bookings" "No file generated, no new invoices found"
}